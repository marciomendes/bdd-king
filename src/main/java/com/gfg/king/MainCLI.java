package com.gfg.king;

import org.apache.log4j.BasicConfigurator;
import org.qaaut.walnut.utils.NutPropertiesUtils;
import org.qaaut.walnut.utils.NutReportUtils;
import org.qaaut.walnut.utils.NutStringUtils;

public class MainCLI {
	public static void main(String[] args) throws Throwable {

		// default arguments
		String[] myArgs = { 
				"--plugin", "json:target/cucumber_report.json", 
				"--glue", "org.qaaut.walnut", 
				"--glue", "org.qaaut.trest", 
				"--glue", "com.gfg.king" 
		};

		String[] allArgs = NutStringUtils.concatArrays(args, myArgs);

		System.out.println("Used arguments:");
		for (String arg : allArgs) {
			System.out.print(arg + " ");
		}
		System.out.println("\n");

		// start the configuration (log4j)
		BasicConfigurator.configure();

		// define if execution is in debug
		NutPropertiesUtils.execInDebug = false;

		// call cucumber api with arguments
		byte exitstatus = cucumber.api.cli.Main.run(allArgs, Thread.currentThread().getContextClassLoader());

		// generate report before close execution
		NutReportUtils.generateReport();
		
		//close execution
		System.exit(exitstatus);		
	}
}

// java -jar nomeDoJar.jar --tags @tagDoTeste classpath:resources/feature -f pretty