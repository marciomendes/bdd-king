Feature: King First Test

  @king
  Scenario: Setup Scenario
    Given user set the properties file './bdd_king_java.properties'
      And user loads the configuration data

  @king
  Scenario Outline: Load Feature Variables
    Given user stores the value '<varValue>' in variable '<varName>'

    Examples: 
      | varName     | varValue                                    |
      | "site_url"  | "http://sandbox-king.aws.dafitierp.com.br/" |
      | "site_user" | "marcio.mendes"                             |
      | "site_pass" | "marcio.mendes"                             |

  @king
  Scenario: Login into king system
    Given web browser is opened
     When user navigates to '${vars.site_url}'
      And user fills by replacing text 'KingLogin-txtUsuario' with '${vars.site_user}'
      And user fills by replacing text 'KingLogin-txtSenha' with '${vars.site_pass}'
      And user clicks 'KingLogin-btnEntrar'
     Then user waits for page to load
      And user saves a screenshot '${vars.folder_default}|king_first_page'
