package com.gfg.king;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/main/resources", 
				 tags = "@king", 
				 plugin= {"json:target/cucumber_report.json"},
				 glue = {						 
						 "org.qaaut.walnut", 	// referencia a jar externa walnut framework
						 "org.qaaut.trest", 	// referencia a jar externa TRest framework
						 "com.gfg.king",		// referencia a CustomFeatureSteps.java interna						 
						}
				)

public class JUnitWrapperTest {

}
